package programodawca.pl.votingsystem.entity;

import javax.persistence.*;

@Entity
@Table(name = "candidates")
public class Candidate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "image_url")
    private String imageUrl;

    @Column(name = "candidate_name")
    private String name;

    @Column(name = "title")
    private String title;

    @Column(name = "number_of_votes")
    private Long numberOfVotes;

    public Candidate() {

    }

    public Candidate(String imageUrl, String name, String title, Long numberOfVotes) {
        this.imageUrl = imageUrl;
        this.name = name;
        this.title = title;
        this.numberOfVotes = numberOfVotes;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getNumberOfVotes() {
        return numberOfVotes;
    }

    public void setNumberOfVotes(Long numberOfVotes) {
        this.numberOfVotes = numberOfVotes;
    }


    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}