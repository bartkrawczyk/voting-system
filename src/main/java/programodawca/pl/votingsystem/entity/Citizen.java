package programodawca.pl.votingsystem.entity;

import javax.persistence.*;

@Entity
@Table(name = "citizens")
public class Citizen {

    @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column(name = "citizen_name")
    private String name;

    @Column(name = "has_voted")
    private Boolean hasVoted;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getHasVoted() {
        return hasVoted;
    }

    public void setHasVoted(Boolean hasVoted) {
        this.hasVoted = hasVoted;
    }

    public Citizen(String name) {
        this.name = name;
    }

    public Citizen() {

    }
}
