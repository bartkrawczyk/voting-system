INSERT INTO citizens(citizen_name, has_voted) VALUES('Tom', false);
INSERT INTO citizens(citizen_name, has_voted) VALUES('David', false);
INSERT INTO citizens(citizen_name, has_voted) VALUES('Olivier', false);
INSERT INTO citizens(citizen_name, has_voted) VALUES('Bart', false);
INSERT INTO candidates(image_url, candidate_name, title, number_of_votes) VALUES('Napoleon_pic.jpg', 'Napoleon Bonaparte', 'Emperor of the French Empire', 0);
INSERT INTO candidates(image_url, candidate_name, title, number_of_votes) VALUES('Alexander_pic.jpg', 'Alexander the Great', 'The Greatest Ruler', 0);